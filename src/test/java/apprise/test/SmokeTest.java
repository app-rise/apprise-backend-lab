package apprise.test;

import static apprise.backend.api.ApiConstants.info;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.junit.Assert.assertEquals;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.typeOf;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.apprise.backend.Testbase;

public class SmokeTest extends Testbase {

	
	@Test
	public void exposes_info() {

		Response response = at(info).get();

		assertEquals(SUCCESSFUL, typeOf(response));
	}

}
