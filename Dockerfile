FROM adoptopenjdk/openjdk11:alpine-jre

COPY lib /code/lib
COPY ${project.build.finalName}.jar /code/

ENTRYPOINT ["java","-jar", "code/${project.build.finalName}.jar"]